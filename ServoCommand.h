/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#ifndef H_SERVO_COMMAND
#define H_SERVO_COMMAND

struct ServoCommand
{
  u8 servoNum = 0;
  u16 servoVal = SERVOMID;

  ServoCommand(){}

  ServoCommand(u8 servoNum, u8 servoValRaw)
    : servoNum(servoNum)
  {
      servoVal = constrain(
        map(servoValRaw, 0, 0b01111111, SERVOMIN, SERVOMAX),
        SERVOMIN, SERVOMAX
      );
  }
};

#endif