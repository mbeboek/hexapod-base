/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "CommandQueue.h"

void CommandQueue::clear()
{
    pos = 0;
}

void CommandQueue::execute(LegServo legServos[9])
{
    for(int i=0; i<pos; ++i)
    {
        legServos[queue[i].servoNum].moveTo(queue[i].servoVal);
    }
}

void CommandQueue::push(ServoCommand cmd)
{
    queue[pos++] = cmd;
}