/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#ifndef H_LEG_SERVO
#define H_LEG_SERVO

#include <Adafruit_PWMServoDriver.h>

#define SERVOMIN  200
#define SERVOMID  375
#define SERVOMAX  600

// standing = ServoLegKeyframe{-1, 455, 465}; // standing pos, legs supporting the hexapod

class LegServo
{
    private:
        Adafruit_PWMServoDriver &pwm;
        u8 index;
        int value = SERVOMID;

    public:
        LegServo(Adafruit_PWMServoDriver &pwm, int index)
            : pwm(pwm), index(index)
        {}

        void moveTo(int val);
};

#endif