## Hexapod - Legs

### Protocol

The legs are controlled via USB (Serial).
Right after startup, a byte containing the character 'R' is sent to signalize that the board is ready.
After that, command and data bytes can be sent to control the legs.
The normal workflow is to send a few control and data bytes, which are buffered, and at the end a special byte to execute them at once.
This means each servo can be controlled at without delay.

#### Byte Format

There are to types of bytes, a command and data byte. <br/>
Command bytes can start/end a command chain, or set the current servo to control.
Data bytes only set a value with a 7-bit precision.<br/>

To differentiate between the two, bit-0 is used.
A '0' marks a command-byte, '1' a data byte.
> **Note:** Bits are labeled starting from the MSB with 0 to 7

##### Command Byte

There are 3 command bytes, defined by bits-1 and 2

```
00 - Clear old and start new Command chain
01 - Set the current servo
10 - Execute all commands and clear the buffer
11 - <unused>
```

If setting the current servo, bits 3-7 are used for the servo number.<br/>
For all other commands, bits 3-7 are set to '1'.

##### Data Bytes

For data bytes, bits 1-7 are used as an simple integer.<br/>
In the case of the servo, this value is mapped to the minimum and maximum angle of the servo.


#### Example

This example sets servo 0, 1 and 2 to the minimum, default and maximum position respectively.
```
  Bits      Comment
00111111    Start a new command chain
00100000    Select servo #0...
10000000    ... and set value to 0 (-180°)
00100001    Select servo #1...
11000000    ... and set value to 64 (0°)
00100001    Select servo #2...
11111111    ... and set value to 127 (+180°)
01011111    Execute all commands
```