/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "LegServo.h"

void LegServo::moveTo(int val)
{
    if(value != val)
    {
        value = val;
        pwm.setPWM(index, 0, value);
    }
}
