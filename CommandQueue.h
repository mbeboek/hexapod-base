/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#ifndef H_COMMAND_QUEUE
#define H_COMMAND_QUEUE

#include "LegServo.h"
#include "ServoCommand.h"

#define COMMAND_QUEUE_SIZE 128

class CommandQueue
{
    private:
        int pos = 0;
        ServoCommand queue[COMMAND_QUEUE_SIZE];

    public:
        void clear();
        void execute(LegServo legServos[9]);
        void push(ServoCommand cmd);
};

#endif