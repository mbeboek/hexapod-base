/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#define SERVO_COUNT 6 * 3
#define CMD_READY 'R' // this is sent out to indicate its ready for commands

#include "ServoCommand.h"
#include "LegServo.h"
#include "CommandQueue.h"

class Main
{
  private:
    Adafruit_PWMServoDriver pwmA = Adafruit_PWMServoDriver();
    // Adafruit_PWMServoDriver pwmB = Adafruit_PWMServoDriver(); // @TODO use diff. address

    LegServo legServos[9] = {
      LegServo(pwmA, 0), LegServo(pwmA, 1), LegServo(pwmA, 2),
      LegServo(pwmA, 4), LegServo(pwmA, 5), LegServo(pwmA, 6),
      LegServo(pwmA, 8), LegServo(pwmA, 9), LegServo(pwmA, 10),
    };

    int currentServo = 0;
    CommandQueue commandQueue;    

  public:

    void main()
    {
      Serial.begin(57600);
      
      pwmA.begin();  
      pwmA.setPWMFreq(60);

      delay(100);
      Serial.write(CMD_READY);

      for(;;)
      {
        update();
        delay(10);
      }
    }

    inline void handleByte(u8 data)
    {
        // Value Byte
        if(data & 0b10000000)
        {
          u8 value = data & 0b01111111;
          commandQueue.push(ServoCommand(currentServo, value));

        // Command Byte  
        } else {
          u8 cmdType = ((data >> 5) & 0b11);
          switch(cmdType)
          {
            case 0b00: // Clear Queue, start new command chain
              commandQueue.clear();
            break;
            case 0b01: // Set current servo
              currentServo = data & 0b00011111;
              if(currentServo >= SERVO_COUNT) {
                currentServo = 0;
              }
            break;
            case 0b10: // Execute current queue and clear
              commandQueue.execute(legServos);
              commandQueue.clear();
            break;
            case 0b11: // unused
            break;
          }
        }
    }

    inline void update()
    {
      while (Serial.available()) 
      {
        handleByte(Serial.read());
      }
    }
};

void setup() {   
  auto mainProg = Main();
  mainProg.main(); 
}
void loop(){}
